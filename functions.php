<?php

// variables

$serverName = "localhost";
$userName = "root";
$password = "";
$db = "forumDb";

// SQL server controll
function ConnectToServer($serverName, $userName, $password)
{
    try {
        $conn = new PDO("mysql:host=$serverName", $userName, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "Connected successfully <br />"; 

    } catch (PDOException $e) {
        echo "Connection failed: " . utf8_encode($e->getMessage()) . "<br />";
    }
}
// Create database
function CreateDatabase($serverName, $userName, $password, $db)
{


    try {
        $conn = new PDO("mysql:host=$serverName", $userName, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "CREATE DATABASE $db
                    CHARACTER SET utf8
	                COLLATE utf8_hungarian_ci;";
        // use exec() because no results are returned
        $conn->exec($sql);
        // echo "Database created successfully<br>";
    } catch (PDOException $e) {
            // echo  "Create failed: " . utf8_encode($e->getMessage()) .  "<br />";
    }

    $conn = null;
}
// Create table
function CreateTable($serverName, $userName, $password, $db, $sql)
{
    try {
        $conn = new PDO("mysql:host=$serverName;dbname=$db", $userName, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        // sql to create table from $sql parameter
        

    
        // use exec() because no results are returned
        $conn->exec($sql);
        // echo "Table $sql created successfully";
    } catch (PDOException $e) {
        // echo $sql . "<br>" . $e->getMessage();
    }

    $conn = null;
}
function AddToDatabase($serverName, $userName, $password, $db, $sql)
{
    try {
        $conn = new PDO("mysql:host=$serverName;dbname=$db", $userName, $password);
            // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            // use exec() because no results are returned
        $conn->exec($sql);
            // echo "New record created successfully";
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

    $conn = null;
}
// ____________________________________________________________


class CommentBlock
{


    public $box = '';

    public function __Construct($items)
    {
        $this->box =
            '<div class="container shadow-lg p-3 mb-5 bg-white rounded" >
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <p class="text-left">#' . ($items["id"]) . '</p>
                        </div>
                        <div class="col">
                            <p class="text-center">Küldte: ' . ($items["nickname"]) . '</p>
                        </div>
                        <div class="col">
                            <p class="text-right">' . ($items["comment_date"]) . '</p>
                        </div>
                        
                    </div>
                </div>
        <div class="card-body">
            ' . ($items["comment"]) . '
        </div>
        </div>
            </div>';

    }
    public function ShowBox()
    {
        echo $this->box;
    }

}
// Getting datas from database

function SelectFromDatabase($serverName, $userName, $password, $db, $tableName)
{
    try {
        $conn = new PDO("mysql:host=$serverName;dbname=$db", $userName, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT * FROM $tableName  ORDER BY comment_date DESC");
        $stmt->execute();
    
        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $comments = $stmt->fetchAll();

        foreach ($comments as $key => $value) {

            $c = new CommentBlock($value);
            $c->ShowBox();
        //         echo "<pre>";
        // print_r($c) ;
        // echo "</pre>";
        }

    } catch (PDOException $e) {
        // echo "Error: " . $e->getMessage();
    }
    $conn = null;


}
?>