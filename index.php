<!DOCTYPE html>
<html lang="hu">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<link rel="stylesheet" href="style.css">
<title>Fórum</title>
</head>
<body>

    <div class="jumbotron ">
        <h1 class="display-4 text-center">Fórum</h1>
        <p class="lead text-center">Beadandó</p>
        <hr class="my-4">
        <b>Kedves Ricsi! Egyik munkatásam, aki úgy írja a PHP kódot, min ahogy Te beszélsz, a következőt mondta:
            "A PHP nyelvet ahhoz tudnám hasonlítani, mint amikor egy vödörbe egyszerre belesz..rnak és belehánynak "
            Na ehhez szólj hozzá!
        </b>
        <p>Csak legyen sql szerver, töltsd ki az űrlapot és dőlj hátra!</p>
        <p>A felépítés lépéseit <a href="https://bitbucket.org/fecsaba/forum/commits/all">itt</a>  megtekintheted </p>
    </div>
    <div class="container-fluid">



    <form method="POST" action="index.php">
        <div class="form-group">
            <label for="nev">*Név:</label>
            <input id="nev" required name="neved"
            value="<?php isset($_POST['neved']) ? $_POST['neved'] : ''; ?>"
            
              class="form-control" type="text">
              
        </div>
        <div class="form-group">
            <label for="comment">*Hozzászólás: </label>
            <textarea id="comment" required name="yourComment" class="form-control" rows="3"></textarea>
        </div>
        <input class="btn btn-primary" type="submit" value="Elküld">

    </form>
    
    </div>
    <?php
    include "functions.php";

    if (isset($_POST["neved"]) && isset($_POST["yourComment"])) {
        // echo $_POST["neved"]."<br />";
        // echo $_POST["yourComment"]."<br />";
        $db_nev = $_POST["neved"];
        $db_comment = $_POST["yourComment"];
        // $db_date = (new DateTime())->format('Y\-m\-d\ h:i:s'); adatbázis elintézi
        
        // $serverName = "localhost";
        // $userName = "root";
        // $password = "";
        // $db = "forumDb";

        $sql[] = "CREATE TABLE ForumTable (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
        nickname VARCHAR(30) NOT NULL,
        
        comment VARCHAR(500) NOT NULL,
        comment_date DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        )";
        // $sql[] = "CREATE TABLE MyGuests (
        //     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
        //     firstname VARCHAR(30) NOT NULL,
        //     lastname VARCHAR(30) NOT NULL,
        //     email VARCHAR(50),
        //     reg_date TIMESTAMP
        //     )";

        ConnectToServer($serverName, $userName, $password);
        CreateDatabase($serverName, $userName, $password, $db);
        for ($i = 0; $i < count($sql); $i++) {
            CreateTable($serverName, $userName, $password, $db, $sql[$i]);
        }



        $sql = "INSERT INTO `forumTable` (nickname, comment)
          VALUES ('$db_nev', '$db_comment')";

        AddToDatabase($serverName, $userName, $password, $db, $sql);



        SelectFromDatabase($serverName, $userName, $password, $db, "ForumTable");


    } else SelectFromDatabase($serverName, $userName, $password, $db, "ForumTable");
    ?>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>